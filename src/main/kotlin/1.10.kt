import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-12
    * DESCRIPTION: Mida d’un String
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val userString = scanner.nextLine()
    val char = scanner.next().single()

    markString(userString, char)
}

fun markString(userString: String, char: Char) {

    repeat(userString.length+4) { print(char) }
    println("")
    print(char)
    repeat(userString.length+2) { print(" ") }
    println(char)
    println("$char $userString $char")
    print(char)
    repeat(userString.length+2) { print(" ") }
    println(char)
    repeat(userString.length+4) { print(char) }
}
