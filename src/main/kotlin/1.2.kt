import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-11
    * DESCRIPTION: String és igual
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val firstString = scanner.next()
    val secondString = scanner.next()

    println(areSameString(firstString, secondString))
}

fun areSameString(firstString: String, secondString: String): Boolean {
    return firstString == secondString
}