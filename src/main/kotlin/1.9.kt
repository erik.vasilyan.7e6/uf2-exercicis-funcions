import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-12
    * DESCRIPTION: Escriu una creu
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val n = scanner.nextInt()
    val char = scanner.next().single()

    if (n >= 3) {
        val cross = writeCross(n, char)
        for (elem in cross) println(elem.toString().replace("[","").replace("]","").replace(",",""))
    }
}

fun writeCross(n: Int, char: Char): MutableList<MutableList<Char>> {

    val cross = mutableListOf<MutableList<Char>>()

    for (i in 0 until n) {
        val line = mutableListOf<Char>()
        if (i == (n/2)) {
            repeat(n) { line.add(char) }
        }
        else {
            repeat(n/2) { line.add(' ') }
            line.add(char)
        }
        cross.add(line)
    }
    return cross
}
