import java.util.Scanner
import kotlin.math.pow

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-11
    * DESCRIPTION: Eleva’l
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val number = scanner.nextInt()
    val power = scanner.nextInt()

    println(powerNumber(number, power))
}

fun powerNumber(number: Int, power: Int): Int {

    return number.toDouble().pow(power.toDouble()).toInt()
}