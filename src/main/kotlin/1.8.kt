import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-11
    * DESCRIPTION: Escriu una línia
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val spaces = scanner.nextInt()
    val n = scanner.nextInt()
    val char = scanner.next().single()

    println(writeLine(spaces, n, char))
}

fun writeLine(spaces: Int, n: Int, char: Char): String {

    var line = ""

    for (i in 0 until spaces) line += " "
    for (j in 0 until n) line += char

    return line
}
