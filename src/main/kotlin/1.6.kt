import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-11
    * DESCRIPTION: Divideix un String
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val userString = scanner.next()
    val userChar = scanner.next().single()

    println(splitStringByChar(userString, userChar))
}

fun splitStringByChar(userString: String, userChar: Char): MutableList<String> {

    val listOfStringParts = mutableListOf<String>()

    var stringPart = ""
    for (char in userString) {
        if (char != userChar) stringPart += char
        else {
            listOfStringParts.add(stringPart)
            stringPart = ""
        }
    }
    if (stringPart != "") listOfStringParts.add(stringPart)
    return listOfStringParts
}