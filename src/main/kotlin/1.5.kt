import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-11
    * DESCRIPTION: D’String a MutableList<Char>
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val userString = scanner.next()

    println(stringToMutableListChar(userString))
}

fun stringToMutableListChar(userString: String): MutableList<Char> {

    val listOfChars = mutableListOf<Char>()

    for (letter in userString) listOfChars.add(letter)

    return listOfChars
}