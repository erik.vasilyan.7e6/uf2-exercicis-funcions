import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-11
    * DESCRIPTION: Mida d’un String
 */

fun main() {
   val scanner = Scanner(System.`in`)

   val word = scanner.nextLine()

    println(lengthOfWord(word))
}

fun lengthOfWord(word: String): Int {
    return word.length
}