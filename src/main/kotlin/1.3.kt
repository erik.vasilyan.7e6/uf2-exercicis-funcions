import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-11
    * DESCRIPTION: Mida d’un String
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val userString = scanner.nextLine()
    val nthCharacter = scanner.nextInt()

    if (nthCharacter > userString.length) println("La mida de l'String és inferior a $nthCharacter")
    else println(stringCharacterAtIndexN(userString, nthCharacter))

}

fun stringCharacterAtIndexN(userString: String, nthCharacter: Int): Char {

    return userString[nthCharacter]
}