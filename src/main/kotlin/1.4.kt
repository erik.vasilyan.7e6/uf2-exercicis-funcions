import java.util.Scanner

/*
    * AUTHOR: Erik Vasilyan
    * DATE: 2022-12-11
    * DESCRIPTION: Subseqüència
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val userString = scanner.nextLine()
    val start = scanner.nextInt()
    val end = scanner.nextInt()

    if (start > userString.length || end > userString.length) println("La subseqüència $start-$end de l'String no existeix")
    else println(subsequenceOfString(userString, start, end))
}

fun subsequenceOfString(userString: String, start: Int, end: Int): String {

    var subsequence = ""
   for (i in start until end) {
       subsequence += userString[i]
   }
    return subsequence
}
